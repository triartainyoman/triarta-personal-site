@extends('layouts.main', ['title' => 'about'])

@section('content')
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>ABOUT <span>ME</span></h1>
        <span class="title-bg">Resume</span>
    </section>

    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <div class="row">
                <!-- Personal Info Starts -->
                <div class="col-12 col-lg-5 col-xl-6">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="text-uppercase custom-title mb-0 ft-wt-600">personal infos</h3>
                        </div>
                        <div class="col-12 d-block d-sm-none">
                            <img src="img/img-mobile.png" class="img-fluid main-img-mobile" alt="my picture" />
                        </div>
                        <div class="col-6">
                            <ul class="about-list list-unstyled open-sans-font">
                                <li> <span class="title">first name :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">I Nyoman</span>
                                </li>
                                <li> <span class="title">last name :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Triarta</span>
                                </li>
                                <li> <span class="title">Age :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">19 Years</span>
                                </li>
                                <li> <span class="title">Nationality :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesian</span>
                                </li>
                                <li> <span class="title">Freelance :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Available</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="about-list list-unstyled open-sans-font">
                                <li> <span class="title">Address :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Tampaksiring</span>
                                </li>
                                <li> <span class="title">phone :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">+6285967153944</span>
                                </li>
                                <li> <span class="title">Email :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">triarta.art@mail.com</span>
                                </li>
                                <li> <span class="title">Instagram :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">trii.arta</span>
                                </li>
                                <li> <span class="title">langages :</span> <span
                                        class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia,
                                        English</span> </li>
                            </ul>
                        </div>
                        <div class="col-12 mt-3">
                            <form action="{{ Route('about.download') }}" method="POST">
                                @csrf
                                <button class="button">
                                    <span class="button-text">Download CV</span>
                                    <span class="button-icon fa fa-download"></span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Personal Info Ends -->
                <!-- Boxes Starts -->
                <div class="col-12 col-lg-7 col-xl-6 mt-5 mt-lg-0">
                    <div class="row">
                        <div class="col-6">
                            <div class="box-stats with-margin">
                                <h3 class="poppins-font position-relative">1</h3>
                                <p class="open-sans-font m-0 position-relative text-uppercase">years of <span
                                        class="d-block">experience</span></p>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="box-stats with-margin">
                                <h3 class="poppins-font position-relative">6</h3>
                                <p class="open-sans-font m-0 position-relative text-uppercase">completed <span
                                        class="d-block">projects</span></p>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="box-stats">
                                <h3 class="poppins-font position-relative">5</h3>
                                <p class="open-sans-font m-0 position-relative text-uppercase">IT<span
                                        class="d-block">certifications</span></p>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="box-stats">
                                <h3 class="poppins-font position-relative">3</h3>
                                <p class="open-sans-font m-0 position-relative text-uppercase">organizational<span
                                        class="d-block">experience</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Boxes Ends -->
            </div>
            <hr class="separator">
            <!-- Skills Starts -->
            <div class="row">
                <div class="col-12">
                    <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">My
                        Skills</h3>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p90">
                        <span>90%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">html & css</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p70">
                        <span>70%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">javascript</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p75">
                        <span>75%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">php</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p60">
                        <span>60%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">laravel</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p55">
                        <span>55%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">jquery</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p70">
                        <span>70%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">flutter</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p70">
                        <span>70%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">dart</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p35">
                        <span>35%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">react</h6>
                </div>
            </div>
            <!-- Skills Ends -->
            <hr class="separator mt-1">
            <!-- Experience & Education Starts -->
            <div class="row">
                <div class="col-12">
                    <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Experience
                        <span>&</span> Education
                    </h3>
                </div>
                <div class="col-lg-6 m-15px-tb">
                    <div class="resume-box">
                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">March - April 2021</span>
                                <h5 class="poppins-font text-uppercase">PROFESSIONAL ACADEMY DIGITAL TALENT SCHOLARSHIP
                                    2021 <span class="place open-sans-font">Ministry of Communication and
                                        Informatics</span>
                                </h5>
                                <p class="open-sans-font">I am a participant in a training held by the Digital Talent
                                    Scholarship, in this training I learned about HTML, CSS, JavaScript (WEB) for 79 hours
                                    of training.</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2020 - 2021</span>
                                <h5 class="poppins-font text-uppercase">himpunan mahasiswa jurusan teknik informatika
                                    <span class="place open-sans-font">Ganesha University of Education</span>
                                </h5>
                                <p class="open-sans-font">Lorem incididunt dolor sit amet, consectetur eiusmod dunt doldunt
                                    dol elit, tempor incididunt</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2020 - 2021</span>
                                <h5 class="poppins-font text-uppercase">Programming Community (Ganesha Pronity) <span
                                        class="place open-sans-font">Ganesha University of Education</span></h5>
                                <p class="open-sans-font">Sharing experiences and knowledge that I have gained in class or
                                    in competitions that I have participated in. Every week I invite my friends to share
                                    about Programming.</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">July - August 2019</span>
                                <h5 class="poppins-font text-uppercase">Pendidikan dan pelatihan three in one junior web
                                    programmer <span class="place open-sans-font">Ministry of Industry</span></h5>
                                <p class="open-sans-font">I am a participant in the Three - In - One Junior Web Programmer
                                    education and training held by BDI Denpasar, in this training I learned about
                                    programming from designing to developing a website for 177 hours of training.</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 m-15px-tb">
                    <div class="resume-box">
                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2019 - present</span>
                                <h5 class="poppins-font text-uppercase">Bachelor Degree <span
                                        class="place open-sans-font">Ganesha University of Education</span></h5>
                                <p class="open-sans-font">S.Kom - Information System</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2016 - 2019</span>
                                <h5 class="poppins-font text-uppercase">Senior High School <span
                                        class="place open-sans-font">SMK Negeri 1 Tampaksiring</span></h5>
                                <p class="open-sans-font">Multimedia Major</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2013 - 2016</span>
                                <h5 class="poppins-font text-uppercase">Junior High School <span
                                        class="place open-sans-font">SMP Negeri 1 Tampaksiring</span></h5>
                                <p class="open-sans-font"></p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2007 - 2013</span>
                                <h5 class="poppins-font text-uppercase">Primary School <span
                                        class="place open-sans-font">SD Negeri 1 Manukaya</span></h5>
                                <p class="open-sans-font"></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Experience & Education Ends -->
        </div>
    </section>
@endsection
