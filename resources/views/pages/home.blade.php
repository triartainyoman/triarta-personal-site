@extends('layouts.main', ['title' => 'home'])

@section('content')
    <section class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
        <div class="color-block d-none d-lg-block"></div>
        <div class="row home-details-container align-items-center">
            <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div>
            <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
                <div>
                    <img src="img/img-mobile.png" class="img-fluid main-img-mobile d-none d-sm-block d-lg-none"
                        alt="my picture" />
                    <h1 class="text-uppercase poppins-font">I'm Triarta.<span>web developer</span></h1>
                    <p class="open-sans-font">I'm a student at Ganesha University of Education and also fullstack web
                        developer.
                        I have experience in developing frontend and backend. With a high curiosity, I like to
                        learn new things, especially about IT and Programming.</p>
                    <a class="button" href="/about">
                        <span class="button-text">more about me</span>
                        <span class="button-icon fa fa-arrow-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
