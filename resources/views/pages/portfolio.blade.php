@extends('layouts.main', ['title' => 'portfolio'])

@section('content')
    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>my <span>portfolio</span></h1>
        <span class="title-bg">works</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="main-content text-center revealator-slideup revealator-once revealator-delay1">
        <div id="grid-gallery" class="container grid-gallery">
            <!-- Portfolio Grid Starts -->
            <section class="grid-wrap">
                <ul class="row grid">
                    <!-- Portfolio Item Starts -->
                    <li>
                        <figure>
                            <img src="img/projects/project-1.png" alt="Portolio Image" />
                            <div><span>Website Project</span></div>
                        </figure>
                    </li>
                    <!-- Portfolio Item Ends -->
                    <!-- Portfolio Item Starts -->
                    <li>
                        <figure>
                            <img src="img/projects/project-2.png" alt="Portolio Image" />
                            <div><span>Website Project</span></div>
                        </figure>
                    </li>
                    <!-- Portfolio Item Ends -->
                    <!-- Portfolio Item Starts -->
                    <li>
                        <figure>
                            <img src="img/projects/project-3.png" alt="Portolio Image" />
                            <div><span>Website Project</span></div>
                        </figure>
                    </li>
                    <!-- Portfolio Item Ends -->
                    <!-- Portfolio Item Starts -->
                    <li>
                        <figure>
                            <img src="img/projects/project-4.png" alt="Portolio Image" />
                            <div><span>Website Project</span></div>
                        </figure>
                    </li>
                    <!-- Portfolio Item Ends -->
                    <!-- Portfolio Item Starts -->
                    <li>
                        <figure>
                            <img src="img/projects/project-5.png" alt="Portolio Image" />
                            <div><span>Mobile Application</span></div>
                        </figure>
                    </li>
                    <!-- Portfolio Item Ends -->
                    <!-- Portfolio Item Starts -->
                    <li>
                        <figure>
                            <img src="img/projects/project-6.png" alt="Portolio Image" />
                            <div><span>Website Project</span></div>
                        </figure>
                    </li>
                    <!-- Portfolio Item Ends -->
                </ul>
            </section>
            <!-- Portfolio Grid Ends -->
            <!-- Portfolio Details Starts -->
            <section class="slideshow">
                <ul>
                    <!-- Portfolio Item Detail Starts -->
                    <li>
                        <figure>
                            <!-- Project Details Starts -->
                            <figcaption>
                                <h3>Website Project</h3>
                                <div class="row open-sans-font">
                                    <div class="col-12 col-sm-6 mb-2">
                                        <i class="fa fa-file-text-o pr-2"></i><span class="project-label">Project </span>:
                                        <span class="ft-wt-600 uppercase">Travel Website</span>
                                    </div>
                                    <div class="col-12 col-sm-6 mb-2">
                                        <i class="fa fa-user-o pr-2"></i><span class="project-label">Client </span>: <span
                                            class="ft-wt-600 uppercase">Personal</span>
                                    </div>
                                    <div class="col-12 col-sm-6 mb-2">
                                        <i class="fa fa-code pr-2"></i><span class="project-label">Langages </span>: <span
                                            class="ft-wt-600 uppercase">React JS</span>
                                    </div>
                                    <div class="col-12 col-sm-6 mb-2">
                                        <i class="fa fa-external-link pr-2"></i><span class="project-label">Preview
                                        </span>: <span class="ft-wt-600 uppercase"><a href="#" target="_blank">-</a></span>
                                    </div>
                                </div>
                            </figcaption>
                            <!-- Project Details Ends -->
                            <!-- Main Project Content Starts -->
                            <img src="img/projects/project-1.png" alt="Portolio Image" />
                            <!-- Main Project Content Ends -->
                        </figure>
                    </li>
                    <!-- Portfolio Item Detail Ends -->
                    <!-- Portfolio Item Detail Starts -->
                    <li>
                        <figure>
                            <!-- Project Details Starts -->
                            <figcaption>
                                <h3>Website Project</h3>
                                <div class="row open-sans-font">
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-file-text-o pr-2"></i><span class="project-label">Project </span>:
                                        <span class="ft-wt-600 uppercase">HMJ TI Website</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-user-o pr-2"></i><span class="project-label">Client </span>: <span
                                            class="ft-wt-600 uppercase">HMJ TI</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-code pr-2"></i><span class="project-label">Langages </span>: <span
                                            class="ft-wt-600 uppercase">CI 3</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-external-link pr-2"></i><span class="project-label">Preview
                                        </span>: <span class="ft-wt-600 uppercase"><a href="https://if.undiksha.ac.id"
                                                target="_blank">if.undiksha.ac.id</a></span>
                                    </div>
                                </div>
                            </figcaption>
                            <!-- Project Details Ends -->
                            <!-- Main Project Content Starts -->
                            <img src="img/projects/project-2.png" alt="Portolio Image" />
                            <!-- Main Project Content Ends -->
                        </figure>
                    </li>
                    <!-- Portfolio Item Detail Ends -->
                    <!-- Portfolio Item Detail Starts -->
                    <li>
                        <figure>
                            <!-- Project Details Starts -->
                            <figcaption>
                                <h3>Website Project</h3>
                                <div class="row open-sans-font">
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-file-text-o pr-2"></i><span class="project-label">Project </span>:
                                        <span class="ft-wt-600 uppercase">Semestakita Website</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-user-o pr-2"></i><span class="project-label">Client </span>: <span
                                            class="ft-wt-600 uppercase">Taksu Tridatu</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-code pr-2"></i><span class="project-label">Langages </span>: <span
                                            class="ft-wt-600 uppercase">Laravel 8</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-external-link pr-2"></i><span class="project-label">Preview
                                        </span>: <span class="ft-wt-600 uppercase"><a href="https://www.semestakita.id"
                                                target="_blank">www.semestakita.id</a></span>
                                    </div>
                                </div>
                            </figcaption>
                            <!-- Project Details Ends -->
                            <!-- Main Project Content Starts -->
                            <img src="img/projects/project-3.png" alt="">
                            <!-- Main Project Content Ends -->
                        </figure>
                    </li>
                    <!-- Portfolio Item Detail Ends -->
                    <!-- Portfolio Item Detail Starts -->
                    <li>
                        <figure>
                            <!-- Project Details Starts -->
                            <figcaption>
                                <h3>Website Project</h3>
                                <div class="row open-sans-font">
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-file-text-o pr-2"></i><span class="project-label">Project </span>:
                                        <span class="ft-wt-600 uppercase">Links Website</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-user-o pr-2"></i><span class="project-label">Client </span>: <span
                                            class="ft-wt-600 uppercase">Personal</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-code pr-2"></i><span class="project-label">Langages </span>: <span
                                            class="ft-wt-600 uppercase">React JS</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-external-link pr-2"></i><span class="project-label">Preview
                                        </span>: <span class="ft-wt-600 uppercase"><a href="https://triarta.netlify.app/"
                                                target="_blank">triarta.netlify.app/</a></span>
                                    </div>
                                </div>
                            </figcaption>
                            <!-- Project Details Ends -->
                            <!-- Main Project Content Starts -->
                            <img src="img/projects/project-4.png" alt="">
                            <!-- Main Project Content Ends -->
                        </figure>
                    </li>
                    <!-- Portfolio Item Detail Ends -->
                    <!-- Portfolio Item Detail Starts -->
                    <li>
                        <figure>
                            <!-- Project Details Starts -->
                            <figcaption>
                                <h3>Mobile Project</h3>
                                <div class="row open-sans-font">
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-file-text-o pr-2"></i><span class="project-label">Project </span>:
                                        <span class="ft-wt-600 uppercase">Flash Shoes</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-user-o pr-2"></i><span class="project-label">Client </span>: <span
                                            class="ft-wt-600 uppercase">Personal</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-code pr-2"></i><span class="project-label">Langages </span>: <span
                                            class="ft-wt-600 uppercase">Laravel, Flutter</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-external-link pr-2"></i><span class="project-label">Preview
                                        </span>: <span class="ft-wt-600 uppercase"><a href="#" target="_blank">-</a></span>
                                    </div>
                                </div>
                            </figcaption>
                            <!-- Project Details Ends -->
                            <!-- Main Project Content Starts -->
                            <img src="img/projects/project-5.png" alt="Portolio Image" />
                            <!-- Main Project Content Ends -->
                        </figure>
                    </li>
                    <!-- Portfolio Item Detail Ends -->
                    <!-- Portfolio Item Detail Starts -->
                    <li>
                        <figure>
                            <!-- Project Details Starts -->
                            <figcaption>
                                <h3>Website Project</h3>
                                <div class="row open-sans-font">
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-file-text-o pr-2"></i><span class="project-label">Project
                                        </span>: <span class="ft-wt-600 uppercase">Todolist Website</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-user-o pr-2"></i><span class="project-label">Client </span>:
                                        <span class="ft-wt-600 uppercase">Personal</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-code pr-2"></i><span class="project-label">Langages </span>:
                                        <span class="ft-wt-600 uppercase">React JS</span>
                                    </div>
                                    <div class="col-6 mb-2">
                                        <i class="fa fa-external-link pr-2"></i><span class="project-label">Preview
                                        </span>: <span class="ft-wt-600 uppercase"><a href="#" target="_blank">-</a></span>
                                    </div>
                                </div>
                            </figcaption>
                            <!-- Project Details Ends -->
                            <!-- Main Project Content Starts -->
                            <img src="img/projects/project-6.png" alt="Portolio Image" />
                            <!-- Main Project Content Ends -->
                        </figure>
                    </li>
                    <!-- Portfolio Item Detail Ends -->
                </ul>
                <!-- Portfolio Navigation Starts -->
                <nav>
                    <span class="icon nav-prev"><img src="img/projects/navigation/left-arrow-light.png"
                            alt="previous"></span>
                    <span class="icon nav-next"><img src="img/projects/navigation/right-arrow-light.png"
                            alt="next"></span>
                    <span class="nav-close"><img src="img/projects/navigation/close-button-light.png" alt="close">
                    </span>
                </nav>
                <!-- Portfolio Navigation Ends -->
            </section>
        </div>
    </section>
    <!-- Main Content Ends -->
@endsection
