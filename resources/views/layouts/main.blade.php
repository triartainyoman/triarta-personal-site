<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{{ ucfirst($title) }} - Triarta Personal Portfolio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Template Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,600i,700" rel="stylesheet">

    <!-- Template CSS Files -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/preloader.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/circle.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link href="{{ asset('css/fm.revealator.jquery.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- CSS Skin File -->
    <link href="css/skins/green.css" rel="stylesheet">

    <!-- Live Style Switcher - demo only -->
    <link rel="alternate stylesheet" type="text/css" title="blue" href="{{ asset('css/skins/blue.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="green" href="{{ asset('css/skins/green.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="yellow" href="{{ asset('css/skins/yellow.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="blueviolet"
        href="{{ asset('css/skins/blueviolet.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="goldenrod"
        href="{{ asset('css/skins/goldenrod.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="magenta" href="{{ asset('css/skins/magenta.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="orange" href="{{ asset('css/skins/orange.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="purple" href="{{ asset('css/skins/purple.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="red" href="{{ asset('css/skins/red.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen"
        href="{{ asset('css/skins/yellowgreen.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styleswitcher.css') }}" />

    <!-- Modernizr JS File -->
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>
</head>

<body class="{{ $title }} light">
    <!-- Live Style Switcher Starts - demo only -->
    <div id="switcher" class="">
    <div class=" content-switcher">
        <h4>STYLE SWITCHER</h4>
        <ul>
            <li>
                <a href="#" onclick="setActiveStyleSheet('purple');" title="purple" class="color"><img
                        src="{{ asset('img/styleswitcher/purple.png') }}" alt="purple" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('red');" title="red" class="color"><img
                        src="{{ asset('img/styleswitcher/red.png') }}" alt="red" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('blueviolet');" title="blueviolet" class="color"><img
                        src="{{ asset('img/styleswitcher/blueviolet.png') }}" alt="blueviolet" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('blue');" title="blue" class="color"><img
                        src="{{ asset('img/styleswitcher/blue.png') }}" alt="blue" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('goldenrod');" title="goldenrod" class="color"><img
                        src="{{ asset('img/styleswitcher/goldenrod.png') }}" alt="goldenrod" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('magenta');" title="magenta" class="color"><img
                        src="{{ asset('img/styleswitcher/magenta.png') }}" alt="magenta" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('yellowgreen');" title="yellowgreen" class="color"><img
                        src="{{ asset('img/styleswitcher/yellowgreen.png') }}" alt="yellowgreen" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('orange');" title="orange" class="color"><img
                        src="{{ asset('img/styleswitcher/orange.png') }}" alt="orange" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('green');" title="green" class="color"><img
                        src="{{ asset('img/styleswitcher/green.png') }}" alt="green" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('yellow');" title="yellow" class="color"><img
                        src="{{ asset('img/styleswitcher/yellow.png') }}" alt="yellow" /></a>
            </li>
        </ul>
        <div id="hideSwitcher">&times;</div>
    </div>
    </div>
    <div id="showSwitcher" class="styleSecondColor"><i class="fa fa-cog fa-spin"></i></div>
    <!-- Live Style Switcher Ends - demo only -->
    <!-- Header Starts -->
    <header class="header" id="navbar-collapse-toggle">
        <!-- Fixed Navigation Starts -->
        <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
            <li class="icon-box {{ $title == 'home' ? 'active' : '' }}">
                <i class="fa fa-home"></i>
                <a href="{{ Route('home') }}">
                    <h2>Home</h2>
                </a>
            </li>
            <li class="icon-box {{ $title == 'about' ? 'active' : '' }}">
                <i class="fa fa-user"></i>
                <a href="{{ Route('about') }}">
                    <h2>About</h2>
                </a>
            </li>
            <li class="icon-box {{ $title == 'portfolio' ? 'active' : '' }}">
                <i class="fa fa-briefcase"></i>
                <a href="{{ Route('portfolio') }}">
                    <h2>Portfolio</h2>
                </a>
            </li>
            <li class="icon-box {{ $title == 'contact' ? 'active' : '' }}">
                <i class="fa fa-envelope-open"></i>
                <a href="{{ Route('contact') }}">
                    <h2>Contact</h2>
                </a>
            </li>
            <li class="icon-box {{ $title == 'video' ? 'active' : '' }}">
                <i class="fa fa-video"></i>
                <a href="{{ Route('video') }}">
                    <h2>Video</h2>
                </a>
            </li>
        </ul>
        <!-- Fixed Navigation Ends -->
        <!-- Mobile Menu Starts -->
        <nav role="navigation" class="d-block d-lg-none">
            <div id="menuToggle">
                <input type="checkbox" />
                <span></span>
                <span></span>
                <span></span>
                <ul class="list-unstyled" id="menu">
                    <li class="{{ $title == 'home' ? 'active' : '' }}"><a href="/"><i
                                class="fa fa-home"></i><span>Home</span></a>
                    </li>
                    <li class="{{ $title == 'about' ? 'active' : '' }}"><a href="{{ Route('about') }}"><i
                                class="fa fa-user"></i><span>About</span></a></li>
                    <li class="{{ $title == 'portfolio' ? 'active' : '' }}"><a href="{{ Route('portfolio') }}"><i
                                class="fa fa-folder-open"></i><span>Portfolio</span></a></li>
                    <li class="{{ $title == 'contact' ? 'active' : '' }}"><a href="{{ Route('contact') }}"><i
                                class="fa fa-envelope-open"></i><span>Contact</span></a></li>
                    <li class="{{ $title == 'video' ? 'active' : '' }}"><a href="{{ Route('video') }}"><i
                                class="fa fa-video"></i><span>Video</span></a></li>
                </ul>
            </div>
        </nav>
        <!-- Mobile Menu Ends -->
    </header>
    <!-- Header Ends -->
    <!-- Content Starts -->
    @yield('content')
    <!-- Content Ends -->

    <!-- Template JS Files -->
    <script src="{{ asset('js/jquery-3.5.0.min.js') }}"></script>
    <script src="{{ asset('js/styleswitcher.js') }}"></script>
    <script src="{{ asset('js/preloader.min.js') }}"></script>
    <script src="{{ asset('js/fm.revealator.jquery.min.js') }}"></script>
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/classie.js') }}"></script>
    <script src="{{ asset('js/cbpGridGallery.js') }}"></script>
    <script src="{{ asset('js/jquery.hoverdir.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

</body>

</html>
