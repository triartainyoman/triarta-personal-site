@extends('layouts.main', ['title' => 'video'])

@section('content')
    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>my <span>video</span></h1>
        <span class="title-bg">posts</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">
            <!-- Articles Starts -->
            <div class="row">
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/qZgKEnFwsws"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/EBwkm8E3-8A"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/fYTOJqmkYtE"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/_RVR4Eetcqc"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/qseAzQ3-qYU"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <!-- Article Ends -->
                <!-- Article Starts -->
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/3Uk1EhkXsbE"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <!-- Article Ends -->
            </div>
            <!-- Articles Ends -->
        </div>
    </section>
@endsection
