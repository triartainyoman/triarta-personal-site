<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        return view('pages.about');
    }

    public function download()
    {
        return response()->download(public_path('files/triarta_cv.pdf'));
    }
}
